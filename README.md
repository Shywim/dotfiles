## Set up

 - Requirements: git

```bash
git clone --bare git@gitlab.com:Shywim/dotfiles $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles checkout
```

If `dotfiles checkout` fail because of existing files, either move these files to a backup directory or use `dotfiles checkout --force`.

```bash
dotfiles config --local status.showUntrackedFiles no
```

This last command hide files that are not explicitely tracked when you type `dotfiles status`.

## Usage

```bash
dotfiles add .zshrc
dotfiles commit -m 'Added .zshrc'
dotfiles push
```

## More

### zsh

 - [Antigen](https://github.com/zsh-users/antigen) is needed to use zsh plugins (available in the AUR as `antigen-git`)

### neovim

 - [vim-plug](https://github.com/junegunn/vim-plug) is needed to use neovim plugins

