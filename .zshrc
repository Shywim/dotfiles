HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory extendedglob nomatch notify
unsetopt autocd beep
bindkey -v

zstyle :compinstall filename '/home/shywim/.zshrc'

autoload -Uz compinit
compinit

[ -f /usr/share/doc/pkgfile/command-not-found.zsh ] && source /usr/share/doc/pkgfile/command-not-found.zsh

if [ -f /usr/share/zsh/share/antigen.zsh ]; then
	source /usr/share/zsh/share/antigen.zsh

	antigen use oh-my-zsh

	antigen bundle git
	antigen bundle mercurial
	antigen bundle zsh-users/zsh-syntax-highlighting

	#antigen theme skylerlee/zeta-zsh-theme
	export SPACESHIP_TIME_SHOW=true
	antigen theme denysdovhan/spaceship-prompt
	antigen apply
fi

systemctl --user import-environment DISPLAY

if [ -x "$(command -v nvim)" ]; then
	export EDITOR=nvim
elif [ -x "$(command -v vim)" ]; then
	export EDITOR=vim
else
	export EDITOR=nano
fi

[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh

export GOPATH=~/go
export PATH=$PATH:~/.yarn/bin:$GOPATH/bin:~/.local/bin:/home/matthieu/.gem/ruby/2.6.0/bin:~/projects/kde/src/kdesrc-build

# Java fix for tiling WMs
#export _JAVA_AWT_WM_NONREPARENTING=1

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

export GTK_USE_PORTAL=1

function start_anaconda() {
	source /opt/anaconda/bin/activate root
}

function stop_anaconda() {
	source /opt/anaconda/bin/deactivate root
}


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
